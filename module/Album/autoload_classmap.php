<?php
return array(
    'Album\Module'                                	=> __DIR__ . '/Module.php',
    'Album\Controller\AlbumController'            	=> __DIR__ . '/src/Album/Controller/AlbumController.php',
    'Album\Model\Album'                        		=> __DIR__ . '/src/Album/Model/Album.php',
    'Album\Model\AlbumTable'                        => __DIR__ . '/src/Album/Model/AlbumTable.php',
);
