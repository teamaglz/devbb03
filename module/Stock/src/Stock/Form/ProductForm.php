<?php
namespace Stock\Form;

use Zend\Form\Form;

class ProductForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('Product');
        
        $this->setAttribute('method', 'post');
        
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Name',
            ),
        ));
        
        $this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type'  => 'textarea',
            ),
            'options' => array(
                'label' => 'Description',
            ),
        ));
        
        $this->add(array(
            'name'    => 'data',
            'type'    => 'Zend\Form\Element\Date',
            'options' => array(
                'label'               => 'Data',
                'create_empty_option' => true,
            ),
        ));

        $this->add(array(
            'name'    => 'data2',
            'type'    => 'Zend\Form\Element\DateSelect',
            'options' => array(
                'label'               => 'Data',
                'create_empty_option' => true,
                'day_attributes'      => array(
                    'data-placeholder' => 'Day',
                    'style'            => 'width: 5%',
                ),
                'month_attributes'    => array(
                    'data-placeholder' => 'Month',
                    'style'            => 'width: 10%',
                ),
                'year_attributes'     => array(
                    'data-placeholder' => 'Year',
                    'style'            => 'width: 6%',
                ),
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Go',
                'id' => 'submitbutton',
                'class' => 'btn btn-primary',
            ),
        ));
    }
}
